class RestaurantHub {
  List<Restaurant> restaurants;

  RestaurantHub({this.restaurants});

  RestaurantHub.fromJson(Map<String, dynamic> json) {
    if (json['restaurants'] != null) {
      restaurants = new List<Restaurant>();
      json['restaurants'].forEach((v) {
        restaurants.add(new Restaurant.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.restaurants != null) {
      data['restaurants'] = this.restaurants.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Restaurant {
  String name;
  String backgroundImageURL;
  String category;
  Contact contact;
  Location location;

  Restaurant(
      {this.name,
      this.backgroundImageURL,
      this.category,
      this.contact,
      this.location});

  Restaurant.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    backgroundImageURL = json['backgroundImageURL'];
    category = json['category'];
    contact =
        json['contact'] != null ? new Contact.fromJson(json['contact']) : null;
    location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['backgroundImageURL'] = this.backgroundImageURL;
    data['category'] = this.category;
    if (this.contact != null) {
      data['contact'] = this.contact.toJson();
    }
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    return data;
  }
}

class Contact {
  String phone;
  String formattedPhone;
  String twitter;
  String facebook;
  String facebookUsername;
  String facebookName;

  Contact(
      {this.phone,
      this.formattedPhone,
      this.twitter,
      this.facebook,
      this.facebookUsername,
      this.facebookName});

  Contact.fromJson(Map<String, dynamic> json) {
    phone = json['phone'];
    formattedPhone = json['formattedPhone'];
    twitter = json['twitter'];
    facebook = json['facebook'];
    facebookUsername = json['facebookUsername'];
    facebookName = json['facebookName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phone'] = this.phone;
    data['formattedPhone'] = this.formattedPhone;
    data['twitter'] = this.twitter;
    data['facebook'] = this.facebook;
    data['facebookUsername'] = this.facebookUsername;
    data['facebookName'] = this.facebookName;
    return data;
  }
}

class Location {
  String address;
  String crossStreet;
  double lat;
  double lng;
  String postalCode;
  String cc;
  String city;
  String state;
  String country;
  List<String> formattedAddress;

  Location(
      {this.address,
      this.crossStreet,
      this.lat,
      this.lng,
      this.postalCode,
      this.cc,
      this.city,
      this.state,
      this.country,
      this.formattedAddress});

  Location.fromJson(Map<String, dynamic> json) {
    address = json['address'];
    crossStreet = json['crossStreet'];
    lat = json['lat'];
    lng = json['lng'];
    postalCode = json['postalCode'];
    cc = json['cc'];
    city = json['city'];
    state = json['state'];
    country = json['country'];
    formattedAddress = json['formattedAddress'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = this.address;
    data['crossStreet'] = this.crossStreet;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['postalCode'] = this.postalCode;
    data['cc'] = this.cc;
    data['city'] = this.city;
    data['state'] = this.state;
    data['country'] = this.country;
    data['formattedAddress'] = this.formattedAddress;
    return data;
  }
}
