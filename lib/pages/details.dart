import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lunch_tyme/models/restauranthub.dart';

class RestaurantDetail extends StatelessWidget {
  final Restaurant restaurant;

  RestaurantDetail(this.restaurant);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: Theme.of(context).iconTheme,
        title: Text(
          'Lunch Tyme',
          style: Theme.of(context).textTheme.title,
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset(
              'images/icon_map.png',
              width: 35,
            ),
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            child: Container(
              height: 180,
              child: GoogleMap(
                buildingsEnabled: true,
                myLocationEnabled: true,
                myLocationButtonEnabled: true,
                mapType: MapType.normal,
                initialCameraPosition: CameraPosition(
                  target:
                      LatLng(restaurant.location.lat, restaurant.location.lng),
                  zoom: 15.5,
                ),
              ),
            ),
          ),
          Container(
            height: 60,
            color: Color(0xff2EA766),
            padding: EdgeInsets.only(left: 12),
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      restaurant.name,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(restaurant.category,
                        style: TextStyle(color: Colors.white, fontSize: 12))
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 26),
            child: Container(
              padding: EdgeInsets.only(left: 12),
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: restaurant.location.formattedAddress
                          .map<Widget>(
                            (line) => Text(
                              line,
                              textAlign: TextAlign.left,
                              style: Theme.of(context).textTheme.body1,
                            ),
                          )
                          .toList(),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 26, left: 12),
            child: Row(
              children: <Widget>[
                restaurant.contact?.formattedPhone?.isNotEmpty ?? false
                    ? Text(restaurant.contact.formattedPhone,
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.body1)
                    : Container(),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                restaurant.contact?.twitter?.isNotEmpty ?? false
                    ? Text("@${restaurant.contact.twitter}",
                        style: Theme.of(context).textTheme.body1)
                    : Container(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
